<?php

namespace app\controllers;

use app\utils\api\RequesicaoApi;
use Yii;
use app\models\Usuario;
use yii\data\ArrayDataProvider;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use function Symfony\Component\String\u;

/**
 * UsuarioController implements the CRUD actions for Usuario model.
 */
class UsuarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Usuario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $api = new RequesicaoApi();
        $result = $api->reqApi()->get('/usuario');

        if ($result->response) {
            $data = Json::decode($result->response);
            $dataProvider = new ArrayDataProvider([
                'allModels' => $data,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'attributes' => [
                        'id_usuario',
                        'nome_usuario',
                        'email_usuario'
                    ],
                ],
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        } else {
            return $this->render('@app/views/site/error', [
                'name' => 'Error',
                'message' => 'Erro ao realizar requisição Rest API. Verifique a sua conexão com o banco de dados'
            ]);
        }
    }

    /**
     * Displays a single Usuario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $api = new RequesicaoApi();
        $result = $api->reqApi()->get('/usuario/' . $id);

        if ($result->response) {
            $data = Json::decode($result->response);
            return $this->render('view', [
                'model' => $data,
            ]);
        } else {
            return $this->render('@app/views/site/error', [
                'name' => 'Error',
                'message' => 'Erro ao realizar requisição Rest API. Verifique a sua conexão com o banco de dados'
            ]);
        }
    }

    /**
     * Creates a new Usuario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($action = null)
    {
        if ($action == 'create-null') {
            $model = new Usuario();

            return $this->renderAjax('create', [
                'model' => $model,
                'acao' => 'create'
            ]);
        } else {
            $req = new RequesicaoApi();
            $api = $req->reqApi();
            $dataUsuario = Yii::$app->request->post("dataUsuario");

            $api->post('/usuario', [
                'nome_usuario' => $dataUsuario['nome_usuario'],
                'email_usuario' => $dataUsuario['email_usuario']
            ]);

            $id = Json::decode($api->get('/usuario')->response);
            $id = end($id)['id_usuario'];

            return $this->redirect(['view', 'id' => $id]);
        }
    }

    /**
     * Updates an existing Usuario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $action = 'update-view')
    {
        $req = new RequesicaoApi();
        $api = $req->reqApi();
        $result = $api->get('/usuario/' . $id);

        if($action == 'update-view') {

            $data = Json::decode($result->response);

            $model = new Usuario();
            $model->id_usuario = $data['id_usuario'];
            $model->nome_usuario = $data['nome_usuario'];
            $model->email_usuario = $data['email_usuario'];

            return $this->render('update', [
                'model' => $model,
                'acao' => 'update'
            ]);
        } else {
            $dataUsuario = Yii::$app->request->post("dataUsuario");

            $api->put('/usuario/' . $id, [
                'nome_usuario' => $dataUsuario['nome_usuario'],
                'email_usuario' => $dataUsuario['email_usuario']
            ]);

            return $this->redirect(['view', 'id' => $id]);
        }
    }

    /**
     * Deletes an existing Usuario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $req = new RequesicaoApi();
        $api = $req->reqApi();
        $result = $api->delete('/usuario/' . $id);

        if ($result->info->http_code == 204) {
            return $this->redirect(['index']);
        } else {
            return $this->render('@app/views/site/error', [
                'name' => 'Error - ' . $result->info->http_code,
                'message' => $result->response_status_lines[0]
            ]);
        }
    }

    /**
     * Finds the Usuario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Usuario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Usuario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
