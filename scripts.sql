-- criar database 'rest-api'

CREATE DATABASE "rest-api"
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'Portuguese_Brazil.1252'
       LC_CTYPE = 'Portuguese_Brazil.1252'
       CONNECTION LIMIT = -1;
	   

-- no schema 'public' criar a tabela 'usuario'

--CREATE SCHEMA public
--  AUTHORIZATION postgres;

--GRANT ALL ON SCHEMA public TO postgres;
--GRANT ALL ON SCHEMA public TO public;
--COMMENT ON SCHEMA public
--  IS 'standard public schema';
  
-- TABLE usuario  

CREATE TABLE usuario
(
  id_usuario serial NOT NULL,
  nome_usuario character varying(100) NOT NULL,
  email_usuario character varying(100) NOT NULL,
  CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario)
)


-- insert de usuários fictícios

INSERT INTO usuario(
            nome_usuario, email_usuario)
    VALUES 
		('Danilo Antônio', 'danilo.antonio@email.com'), 
		('João Paulo', 'joao.paulo@email.com'), 
		('Maria Eduarda', 'maria.eduarda@email.com'), 
		('Carlos Roberto', 'carlos.roberto@email.com'), 
		('Flávio Souza', 'flavio.souza@email.com'), 
		('Pedro Santos', 'pedro.santos@email.com'), 
		('Bruna Alves', 'bruna.alves@email.com'), 
		('Henrique Pereira', 'henrique.pereira@email.com'), 
		('Guilherme Nunes', 'guilherme.nunes@email.com'), 
		('Ana Clara', 'ana.clara@email.com'), 
		('Paula Maria', 'paula.maria@email.com'), 
		('José Pereira', 'jose.pereira@email.com'), 
		('Luciana Santana', 'luciana.santana@email.com'), 
		('Antônio Carlos', 'antonio.carlos@email.com'),
		('Manoel Carlos', 'manoel.carlos@email.com');
