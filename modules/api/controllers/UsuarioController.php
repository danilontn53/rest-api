<?php

namespace app\modules\api\controllers;

use app\models\Usuario;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class UsuarioController extends ActiveController
{
    public $modelClass = 'app\models\Usuario';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'prepareDataProvider' => [$this, 'prepareDataProvider']
        ];
        $actions['view'] = [
            'class' => 'yii\rest\ViewAction',
            'modelClass' => $this->modelClass,
            'findModel' => [$this, 'findModel']
        ];
        $actions['options'] = [
            'class' => 'yii\rest\OptionsAction'
        ];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $query = Usuario::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        return $dataProvider;
    }

    public function findModel($id)
    {
        $model = Usuario::find()->where(['id_usuario' => (int) $id])->one();
        if (!$model) {
            echo json_encode(array(
                'name'=>'Not Found',
                'message'=>'Object not found: ' . $id,
                'code'=>0,
                'status'=>404,
                'type'=>'yii\\web\\NotFoundHttpException',
                )
                ,JSON_PRETTY_PRINT);
        }
        return $model;
    }

}
