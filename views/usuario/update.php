<?php

use app\assets\AppAssetApiUsuario;
use yii\helpers\Html;

AppAssetApiUsuario::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $acao */


$this->title = 'Atualizar Usuario: ' . $model['nome_usuario'];
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model['nome_usuario'], 'url' => ['view', 'id' => $model['id_usuario']]];
$this->params['breadcrumbs'][] = 'Atualizar';

?>
<div class="usuario-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'acao' => $acao
    ]) ?>

</div>
