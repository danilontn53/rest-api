<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsuarioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usuários';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <p class="text_right">
        <?= Html::button('Criar Usuário', [
            'value' => Url::to(['create', 'action' => 'create-null']),
            'class' => 'btn btn-primary btn_azul',
            'id' => 'btnModalCriarUsuario',
        ]) ?>
    </p>

    <?php
    Modal::begin([
        'header' => '<h4>Novo Usuario</h4>',
        'id'     => 'modalCriarUsuario',
        'size'   => 'model-lg',
        'class' => 'modal-dialog'
    ]);

    echo "<div id='modalContentCriarUsuario'></div>";

    Modal::end();
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id_usuario',
                'label' => 'ID'
            ],
            [
                'attribute' => 'nome_usuario',
                'label' => 'Nome'
            ],
            [
                'attribute' => 'email_usuario',
                'label' => 'E-mail'
            ],
            ['class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'header' => 'Ações',
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open">&nbsp;</span>', ['view', 'id' => $model['id_usuario']], [
                            'title' => Yii::t('app', 'Visualizar'),
                        ]);
                    },
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil">&nbsp;</span>', ['update', 'id' => $model['id_usuario'], 'action' => 'update-view'], [
                            'title' => Yii::t('app', 'Atualizar'),
                        ]);
                    },
                    'delete' => function($url, $model){
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $model['id_usuario']], [
                            'class' => '',
                            'title' => Yii::t('app', 'Deletar'),
                            'data' => [
                                'confirm' => 'Tem certeza que deseja excluir este Usuário?',
                                'method' => 'post',
                            ],
                        ]);
                    }
                ],
            ],
        ]
    ]); ?>
</div>

<?php $this->registerJs($this->render('@app/web/js/scripts.js')); ?>

