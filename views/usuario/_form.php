<?php

use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $acao */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="usuario-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nome_usuario')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_usuario')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::button($acao == 'create' ? 'Salvar' : 'Alterar',
            [
                'class' => $acao == 'create' ? 'btn btn-success' : 'btn btn-primary btn_azul',
                'id' => $acao == 'create' ? 'create' : 'update'
            ]
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script type="text/javascript">
    var id = <?= Json::encode($model['id_usuario']);?>;
</script>
