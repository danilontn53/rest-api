<?php

use kartik\dialog\Dialog;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Usuario */

$this->title = $model['nome_usuario'];
$this->params['breadcrumbs'][] = ['label' => 'Usuarios', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="usuario-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="pull-left">
        <?= Html::a('Atualizar', ['update', 'id' => $model['id_usuario']], ['class' => 'btn btn-primary']) ?>

        <?php
        echo Dialog::widget(['overrideYiiConfirm' => true]);

        echo Html::a('Apagar', ['delete', 'id' => $model['id_usuario']], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Tem certeza que deseja excluir este Usuário?',
                'method' => 'post',
            ],
        ]);
        ?>

    </div>

    <div class="pull-right">
        <a href="javascript:history.back()" class="btn btn-default btn_cin">Voltar</a>
    </div>

    <div style="margin-top: 1.5cm">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'id_usuario',
                    'label' => 'ID'
                ],
                [
                    'attribute' => 'nome_usuario',
                    'label' => 'Nome'
                ],
                [
                    'attribute' => 'email_usuario',
                    'label' => 'E-mail'
                ],
            ],
        ]) ?>
    </div>


</div>
