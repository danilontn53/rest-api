<?php

use app\assets\AppAssetApiUsuario;
use yii\helpers\Html;

AppAssetApiUsuario::register($this);
/* @var $this yii\web\View */
/* @var $model app\models\Usuario */
/* @var $acao */

$this->title = 'Criar Usuário';
$this->params['breadcrumbs'][] = ['label' => 'Usuários', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usuario-create">

    <?= $this->render('_form', [
        'model' => $model,
        'acao' => $acao
    ]) ?>

</div>
