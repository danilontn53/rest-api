<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Rest API - Client</h1>
    </div>

    <div class="body-content">
        <div>
            <?php

            echo Html::a('Acessar Painel de Usuários', ['/usuario/index'], [
                'class' => 'btn btn-success btn-lg btn-block'
            ]);
            ?>
        </div>
    </div>
</div>
