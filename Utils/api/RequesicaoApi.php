<?php

namespace app\utils\api;

use RestClient;

class RequesicaoApi
{
    public function reqApi() {

        return new RestClient([
            'base_url' => "http://$_SERVER[HTTP_HOST]" . "/rest-api/api",
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);
    }

}