/* View index Usuario */
$(function(){
    $('#btnModalCriarUsuario').click(function(){
        $('#modalCriarUsuario').modal('show')
            .find('#modalContentCriarUsuario')
            .load($(this).attr('value'));
    });
});
