$(function () {
        $("#update").on("click", function(e){
        var data={
            id_usuario: id,
            nome_usuario: $('#usuario-nome_usuario').val(),
            email_usuario: $('#usuario-email_usuario').val(),
        };

        if (data['nome_usuario'] !== "" && data['email_usuario'] !== "") {
            var url = '/' + window.location.pathname.split('/')[1] + '/usuario/update?id=' + id + '&action=update-save';
            $.ajax({
                url: url,
                data: {
                    dataUsuario: data,
                },
                type: 'POST',
                success: function (data) {
                    window.location = url;
                },
                // error: function (xhr, status, error) {
                //     alert('Ocorreu o seguinte erro ao processar o seu pedido: ' + xhr.responseText);
                // }

            });
        } else {
            validaForm(data);
        }
    });

    $("#create").on("click", function(e){
        var data={
            nome_usuario: $('#usuario-nome_usuario').val(),
            email_usuario: $('#usuario-email_usuario').val(),
        };

        if (data['nome_usuario'] !== "" && data['email_usuario'] !== "") {
            var url = '/' + window.location.pathname.split('/')[1] + '/usuario/create';
            $.ajax({
                url: url,
                data: {
                    dataUsuario: data,
                },
                type: 'POST',
                success: function (data) {
                    window.location = url;
                }
            });
        } else {
            validaForm(data);
        }
    });

    function validaForm(data) {
        if (data['nome_usuario'] === "" || data['email_usuario'] === "") {
            $('#w0').data('yiiActiveForm').submitting = true;
            $('#w0').yiiActiveForm('validate');
        }
    }

});