# Rest API

Rest API é uma aplicação que gerencia dados de usuários, com um CRUD para API (backend) e Client (frontend) para gerenciamento dos usuários.
Na Rest API é possível: listar, cadastrar, alterar e remover um usuário.

## rest-api

[![version](https://img.shields.io/badge/version-1.0.0-blue.svg)](https://semver.org)

REQUISITOS
------------

O requisito mínimo desse modelo de projeto para o servidor da Web:

~~~
- PHP >= 7.3.12
- PostgreSQL 9.4
- Git
- Composer
~~~

INSTALAÇÃO
------------

Necessário clonar o projeto do seguinte repositório do [GitLab](https://gitlab.com/danilontn53/rest-api):

```git
git clone https://gitlab.com/danilontn53/rest-api.git
```

Se você não possui o [Composer](http://getcomposer.org/), pode instalá-lo seguindo as instruções em [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

Após clonar o projeto, deverá ser executado o seguinte comando dentro da raiz do projeto `/rest-api`:

```composer
composer update
```

**NOTAS:** 
- A raiz `/rest-api` deve estar dentro do seu servidor local. 

Agora você pode acessar a aplicação através da seguinte URL, caso o host do seu servidor seja `http://localhost/`:

~~~
http://localhost/rest-api/
~~~

CONFIGURAÇÃO
-------------

### Banco de Dados

Crie o banco de dados manualmente no PostgreSQL seguindo os comandos sql do arquivo `/rest-api/scripts.sql`, que está na raiz do projeto, para criar a database `rest-api` e a tabela `usuario`.

**NOTAS:** 
- Lembre de ativar os seguintes drivers do PHP do seu servidor para permitir o acesso do PHP ao bancos de dados PostgreSQL:
	
~~~
pdo_pgsql
pgsql
~~~
	
Edite o arquivo `config/db.php` com dados reais, por exemplo:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;dbname=rest-api',
    'username' => 'username',
    'password' => 'password',
    'charset' => 'utf8',
];
```

USO
------------

###Requisições (CRUD)

Os exemplos a seguir podem ser executados no aplicativo [PostMan](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) (navegador Google Chrome).
Porém não somente neste aplicativo, podem ser usados outros como SOAP.

- GET /usuario: lista todos os usuários;

	~~~
	Exemplo: http://localhost/rest-api/api/usuario/
	~~~

- POST /usuario: cria um novo usuário;

	~~~
	Exemplo: http://localhost/rest-api/api/usuario
	
	nome_usuario: "Mario José"
		type: string
	email_usuario: "mario.jose@email.com"
		type: string
	~~~
	
- GET /usuario/{id}: retorna os detalhes do usuário pelo id;

	~~~
	Exemplo: http://localhost/rest-api/api/usuario/1
	~~~
	
- PUT /usuario/{id}: atualizar o usuário pelo id;

	~~~
	Exemplo: http://localhost/rest-api/api/usuario/1
	
	nome_usuario: "Mario João"
		type: string
	email_usuario: "mario.joão@email.com"
		type: string
	~~~
	
- DELETE /usuario/{id}: excluir o usuário pelo id;

	~~~
	Exemplo: http://localhost/rest-api/api/usuario/1
	~~~
	

###Client

Na seguinte URL `http://localhost/rest-api/` é exibido um painel de manipulação de usuários, que:

- lista;
- cadastra; 
- altera;
- remove.

INFORMAÇÕES SOBRE O DESENVOLVIMENTO DA APLICAÇÃO
------------

Para o desenvolvimento da Rest API foram utilizadas as seguintes ferramentas e tecnologias:

- [Yii2 Framework](http://www.yiiframework.com/)
- PHP 7.3.12
- PostgreSQL 9.4
- PgAdmin III
- Versionamento git no [GitLab](https://gitlab.com/danilontn53/rest-api)
- PHPStorm IDE

Para os testes das resquisições da api:

- [PostMan](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop) (navegador Google Chrome)

ESTRUTURA DO DIRETÓRIO
-------------------

      assets/             contém definições de assets
      commands/           contém console commands (controllers)
      config/             contém configuração da aplicação
      controllers/        contém controller classes
      mail/               contém visualização de arquivos para e-mails
      models/             contém modelos de classes
      modules/            contém controle da API
      runtime/            contém arquivos gerados durante o tempo de execução
      tests/              contém various tests for the basic application
      vendor/             contém dependências e pacotes
      views/              contém views da aplicação
      web/                contém scripts de entrada e os recursos da Web


